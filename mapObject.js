function mapObject(elements, cb) {
  let NewObject = {};
  for (let key in elements) {
    NewObject[key] = cb(elements[key]);
  }
  return NewObject;
}

module.exports = mapObject;
