function invert(object) {
  let invert = {};

  for (let key in object) {
    invert[object[key]] = key;
  }
  return invert;
}

module.exports = invert;
