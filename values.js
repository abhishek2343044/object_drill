function values(obj) {
  let values = [];
  let string = JSON.stringify(obj).replace('{', '').replace('}', '');

  let data = string.split(',');
  for (let index = 0; index < data.length; index += 1) {
    let value = data[index].split(':');
    values.push(value[1]);
  }
  return values;
}

module.exports = values;
