function pairs(elements) {
  let pairs = [];

  for (let key in elements) {
    let pair = [];
    pair.push(key);
    pair.push(elements[key]);

    pairs.push(pair);
  }
  return pairs;
}

module.exports = pairs;
