function defaults(object, defaultObject) {
  //let NewArray = {};
  for (let key in defaultObject) {
    if (!object[key]) {
      object[key] = defaultObject[key];
    }
    //NewArray[key] = cb(elements[key]);
  }
  return object;
}

module.exports = defaults;
