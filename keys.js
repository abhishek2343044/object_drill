function keys(obj) {
  let keys = [];
  let string = JSON.stringify(obj).replace('{', '').replace('}', '');

  let data = string.split(',');
  for (let index = 0; index < data.length; index += 1) {
    let key = data[index].split(':');
    keys.push(key[0]);
  }
  return keys;
}

module.exports = keys;
